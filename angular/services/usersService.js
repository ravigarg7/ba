BA.factory("UserService", ["$http", "$q", "$localStorage", "localStorageService",
                           function($http, $q, $localStorage, localStorageService) {
    var api_url = "http://localhost:8000/"
    return {
        login: function(data) {
            var url = api_url + "o/token/?grant_type=password&username="+data.username+"&password="+data.password+"&client_id=4v6q0zvR4duWJx8CuIt8aRcyRJ4n4vunEDFBw3Cf&client_secret=keTampIiFdLGCMbwrUoBkmmifSWwYu2XgzM1WSVpIKJhCdhNJCt31Oi8ianZ4bwhbQqorOXNarqFsRX3MCHZgIbiYAvVjmFEyeononlbKPTpbaGqZvys6OJ8kt3b6EBW";
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: url,
                data: data
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        info: function(entityId) {
            var url = api_url + entityId;
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        groups: function(entityId) {
            var url = api_url + entityId + "/groups/";
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
           }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        add_groups: function(group_ids) {
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: api_url + entityId + "/groups/",
                data: group_ids
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        all: function() {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: api_url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        add: function(entity) {
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: api_url,
                data: entity
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(data);
            });
            return defer.promise;
        },
        update: function(entity) {
            var url = api_url + entity.id + "/";
            var defer = $q.defer();
            $http({
                method: 'PUT',
                url: url,
                data: entity
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        delete: function(entity_id) {
            var url = api_url + entity_id + "/";
            var defer = $q.defer();
            $http({
                method: 'DELETE',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        entityGroups: function() {
            var url = api_url + "entity_groups/";
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {

            }).error(function(data, status, header, config) {

            });
            return defer.promise;
        },
        loggedIn: function() {
            var currentUser = localStorageService.get('currentUser')
            var defer = $q.defer();
            if ( currentUser.access_token ) {
                defer.resolve(currentUser);
            } else {
                defer.reject('404');
                $state.go('tabs.home')
            }
            return defer.promise;            
        },
    }
}]);