var BA = angular.module('ionicApp', ['ionic','ngStorage', 'LocalStorageModule'])

.config(function($stateProvider, $urlRouterProvider, localStorageServiceProvider) {
  $stateProvider
    .state('tabs', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html"
    })
    .state('tabs.home', {
      url: "/home",
      views: {
        'home-tab': {
          templateUrl: "templates/home.html",
          controller: 'HomeTabCtrl',
        }
      }
    })
    .state('tabs.facts', {
      url: "/facts",
      views: {
        'home-tab': {
          templateUrl: "templates/facts.html"
        }
      }
    })
    .state('tabs.facts2', {
      url: "/facts2",
      views: {
        'home-tab': {
          templateUrl: "templates/facts2.html"
        }
      }
    })
    .state('tabs.about', {
      url: "/about",
      views: {
        'about-tab': {
          templateUrl: "templates/about.html"
        }
      }
    })
    .state('tabs.navstack', {
      url: "/navstack",
      views: {
        'about-tab': {
          templateUrl: "templates/nav-stack.html"
        }
      }
    })
    .state('tabs.contact', {
      url: "/contact",
      views: {
        'contact-tab': {
          templateUrl: "templates/contact.html"
        }
      }
    })
    .state('tabs.register', {
      url: "/register",
      views: {
        'register-tab': {
          templateUrl: "templates/register.html"
        }
      }
    })
    .state('tabs.dashboard', {
      url: "/dashboard",
      views: {
        'dashboard-tab': {
          controller: 'DashboardCtrl',
          templateUrl: "templates/dashboard.html"
        }
      }
    }).state('tabs.logout', {
      url: "/logout",
      views: {
        'logout-tab': {
          controller: 'LogoutCtrl',
          templateUrl: "templates/logout.html"
        }
      }
    });
    $urlRouterProvider.otherwise("/tab/home");
    localStorageServiceProvider.setPrefix('BA');    
});