BA.controller('HomeTabCtrl', ['$scope', '$rootScope','$location','$q','UserService','$ionicModal','$localStorage','localStorageService', '$state',
                               function($scope, $rootScope, $location, $q, UserService, $ionicModal,$localStorage, localStorageService, $state){
    $rootScope.currentUser = localStorageService.get('currentUser');
    console.log($rootScope.currentUser);
    $scope.openLogin = function() {
        $ionicModal.fromTemplateUrl('templates/login.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
            $scope.modal.show();
            $scope.closeModal = function(){
                $scope.modal.hide();
            }
            $scope.items = {};
            $scope.submit = function() {
                $scope.login = function() {
                    return UserService.login($scope.items);
                }
                $scope.response = $scope.login()
                $scope.response.then(function(data){
                    $localStorage.data = data
                    localStorageService.set('currentUser', data);
                    $rootScope.currentUser = localStorageService.get('currentUser');
                    console.log($rootScope.currentUser);
                    if ( $rootScope.currentUser.access_token ) {
                        $scope.modal.hide();
                        $state.go('tabs.dashboard')                    
                    } else {
                        
                    }                    
                },function(error) {
                    console.log('My first promise failed', error);
                });                
            };
            console.log($rootScope.currentUser);
        });  
    }
    $scope.openRegister = function() {
        $ionicModal.fromTemplateUrl('templates/register.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
            $scope.modal.show();
            console.log(localStorageService.get('currentUser'));
            $scope.closeModal = function(){
                $scope.modal.hide();
            }
        });
    }
}]);

BA.controller('DashboardCtrl', ['$scope', '$rootScope','$location','$q','UserService','$ionicModal','$localStorage','localStorageService', '$state',
                                function($scope, $rootScope, $location, $q, UserService, $ionicModal,$localStorage, localStorageService, $state){
    $rootScope.currentUser = localStorageService.get('currentUser');
    console.log($rootScope.currentUser);
    if ( $rootScope.currentUser.access_token ) {
        //$state.go('tabs.home')
    } else {
        //$state.go('tabs.home')
    }
    
}]);

BA.controller('LogoutCtrl', ['$scope', '$rootScope','$location','$q','UserService','$ionicModal','$localStorage','localStorageService', '$state',
                                function($scope, $rootScope, $location, $q, UserService, $ionicModal,$localStorage, localStorageService, $state){
    $rootScope.currentUser = localStorageService.get('currentUser');
    localStorageService.remove('currentUser');
    $state.go('tabs.home');
    $scope.logout = function() {
        
    }
}]);